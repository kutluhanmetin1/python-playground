# list
numbers = [1,2,3,'four']
print(numbers[0])                       # get
print(len(numbers))                     # length
numbers.append(5)                       # add
for number in numbers:                  # iterate
    print(number)

# tuple -> immutable list
tupleNumbers = (1,2,3,'four')
for tupleNumber in tupleNumbers:        # iterate
    print(tupleNumber)

# dictionary -> hashmap
students = {'A': 1, 'B': 2}
print(students['A'])                    # get
students['C'] = 3                       # add
students.pop('A')                       # remove
print('A' in students)                  # check if key exists
print(students.keys())                  # keys
print(students.values())                # values
for key in students.keys(): # iterate
    print(key, ' ', students[key])

# condititional
if 'A' in students.keys():
    print('yes')
elif 'A' not in students.keys():
    print('false')

# for loop
for i in range(0, 5):
    print(i)
for i in range(0, 5, 1):
    print(i)
for i in range(5, 0, -1):
    print(i)

# function
def add(x, y):
    return x+y

# exception
try:
    print(numbers[10])
except:
    print('no index found')

# class
class MyClass:
    words = []

    def __init__(self, data=["baz"]): # constructor
        self.words = data

    def count(self): # self -> this
        return len(self.words)

clz = MyClass()
print(clz.count())
print(clz.words)

clz = MyClass(["foo","bar"])
print(clz.count())
print(clz.words)